# Simulador de Automatas Finitos

El proyecto simula el comportamiendo de los automatas finitos, es decir, primero es necesario proporcionar los 5 elementos de un automata (conjunto finito de estados, alfabeto, estado inicial, función de transición y estados de aceptacion) a traves de la interface (html). Una vez contruido el automata, se podra ingresar una cadena y determinar si es, o no, aceptada.

## Construido con 🛠️

* JavaScript - Lenguaje de programación
* HTML5 - Lenguaje de etiquetado
* [Jquery](https://jquery.com/) - Biblioteca para simplificar la interacción del DOM
* [bootstrap](https://getbootstrap.com/) - Estilos Front-End


## Estado
#### Finalizado sin continuidad

## Notas

El proyecto contiene codigo spaghetti. Fue pensado para uso personal y se desarrolló en 5 dias, por lo que no contiene buenas practicas ni codigo limpio.

## Imagenes de Ejemplo
![alt text](./imgs/automatas.png)
![alt text](./imgs/automatas2.png)
