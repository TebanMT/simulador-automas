var NUM_NODOS;
var NODOS_ARRAY = [];
var ARRAY_ALFABETO;
var EDGES = [];
var ARRAY_DICT_NODOS = []
var ESTADO_ACEPTACION = []

function generateDictToDraw() {
    ARRAY_DICT_NODOS = []
    for (let i = 0; i < NUM_NODOS; i++) {
        ARRAY_DICT_NODOS.push({id:i , label:"Q"+i, color:"#97C2FC"})
    }
}

function eliminarCajaResumen(div){
    //Si existe la caja o el div...
    if(div !== null){
        while (div.hasChildNodes()){
            div.removeChild(div.lastChild);
        }
    }else{
        alert ("No existe el div trancisiones.");
    }
}

function generateArrayNodes() {
    for (let i = 0; i < NUM_NODOS; i++) {
        NODOS_ARRAY.push("Q"+i)
    }
}

//1
function generateTransiciones() {
    NODOS_ARRAY = []
    EDGES = []
    // Obtener la referencia del elemento body
    var div = document.getElementById("transiciones")
    eliminarCajaResumen(div)//elimina el contenido del div
    // obtener numero de nodos
    NUM_NODOS = document.getElementById("numNodos").value;
    generateDictToDraw(); // se genera el array de objetos Vis
    generateArrayNodes(); // se genera array de los NODOS
    //obtiene el alfabeto en su forma x1,x2,x3,.....xn
    var setAlfabeto = document.getElementById("alfabeto").value;
    // se transforma el alfabeto en una lista de simbolos
    ARRAY_ALFABETO = setAlfabeto.split(',') // se asignan los elementos del alfabeto a la varible global ARRAY_ALFABETO
    // si no es un numero error
    if (parseInt(NUM_NODOS) <= 0) {
        alert("El Numero de Nodos no puede ser 0 o menor de 0")
        return
    }
    //si contiene elemntos vacios no puede continuar
    if (ARRAY_ALFABETO.includes('')) {
        alert("El alfabeto no puede contener elemnetos vacios: del tipo ',,'")
        return
    }
    // Crea un elemento <table> y un elemento <tbody>
    var tabla   = document.createElement("table");
    tabla.setAttribute("class","table table-hover")
    var tblBody = document.createElement("tbody");

    // Crea las celdas
    for (var i = 0; i < parseInt(NUM_NODOS)+1; i++) {
      // Crea las hileras de la tabla
      var hilera = document.createElement("tr");
      for (var j = 0; j < ARRAY_ALFABETO.length+1; j++) {
        // Crea un elemento <td> y un nodo de texto, haz que el nodo de
        // texto sea el contenido de <td>, ubica el elemento <td> al final
        // de la hilera de la tabla
        var celda = document.createElement("td");
        celda.setAttribute("scope","row")

        if (i == 0 & j>0) { // cada celda de la primera fila es un elemento del alfabeto
            var textoCelda = document.createTextNode(ARRAY_ALFABETO[j-1]);
            celda.appendChild(textoCelda);
        }else{
            if(i>0 & j == 0){ //cada celda de la primera columna es un nodo
                var textoCelda = document.createTextNode("Q"+(i-1));
                celda.appendChild(textoCelda);
            }else{
                if (i>0) { // inputs para ingresar las transiciones
                    //var input = document.createElement("input");
                    var select = document.createElement("select")
                    select.setAttribute("class","form-control")
                    var opt = document.createElement('option');
                    opt.appendChild(document.createTextNode(""));
                    select.appendChild(opt);
                    for(k = 0 ; k < NODOS_ARRAY.length; k++){
                        var opt = document.createElement('option');
                        opt.appendChild(document.createTextNode(NODOS_ARRAY[k]));
                        select.appendChild(opt); 
                    }
                    //celda.appendChild(input);
                    celda.appendChild(select);
                    select.setAttribute("value","")
                    select.setAttribute("id","t")
                }
            }
        }
        hilera.appendChild(celda);
      }
      // agrega la hilera al final de la tabla (al final del elemento tblbody)
      tblBody.appendChild(hilera);
    }
    // posiciona el <tbody> debajo del elemento <table>
    tabla.appendChild(tblBody);
    // appends <table> into <body>
    div.appendChild(tabla);
    // modifica el atributo "border" de la tabla y lo fija a "2";
    tabla.setAttribute("border", "2");
    tabla.setAttribute("id", "tabla_data");
    tabla.style.paddingBottom = "10px";
    //se agrega boton para agregar las transiciones al grafo
    const button = document.createElement("button")
    button.setAttribute("class","btn btn-primary")
    let text = document.createTextNode("Agregar transiciones")
    button.appendChild(text)
    button.setAttribute("onclick","addTransiciones()")
    div.appendChild(button)
    draw();
}

//2
function addTransiciones() {
    EDGES = [] // siempre se vacia el array EDGES para que cada que se modifiquen las trancisiones se generan nuevas y no se sumen a las anteriores
    var e = document.getElementById("tabla_data")
    var elementosTR=e.parentElement.getElementsByTagName("tr");
    // recorremos cada uno de los elementos del array de elementos <td>
    for(let i=0;i<elementosTR.length;i++)
    {
        var elementosTD=elementosTR[i].getElementsByTagName("td"); // se obtienen los elemntos TD de cada TR
        for (let j = 0; j < elementosTD.length; j++) {
            const input = elementosTD[j].getElementsByTagName("select").item(0) // se optienen solo los inputs de cada TD
            if ( input != null & i>0) { // si el input es diferente de nulo y no sea la primera fila(la primer fila nunca tiene inputs) agrega un objeto Vis a EDGES
                EDGES.push({from    :   NODOS_ARRAY.indexOf(NODOS_ARRAY[i-1]), // Nodo origen (index) columna 0 fila mayor a 0
                            to      :   NODOS_ARRAY.indexOf(input.value), // Nodo destino index valor del input
                            label   :   elementosTR[0].cells[j].innerHTML, // label del edge dado por los valores la la fila 0
                            color   :   "#97C2FC",
                            arrows  :   "to"})
            }
        }
    }
    var estadosIyF = document.getElementById("estadosIyF");
    estadosIyF.style.display = "";
    draw();

}

function draw() {
    // create an array with nodes
    
    var nodes = new vis.DataSet(ARRAY_DICT_NODOS);

    // create an array with edges
    var edges = new vis.DataSet(EDGES);
    // create a network
    var container = document.getElementById("network");
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {};
    var network = new vis.Network(container, data, options);

}

//3
function paintStates() {
    ESTADO_ACEPTACION = []
    generateDictToDraw();
    //aux_paint_start_estate();
    aux_paint_end_estate();
    ARRAY_DICT_NODOS[0].color = "#008000"
    if(ESTADO_ACEPTACION.includes(ARRAY_DICT_NODOS[0].label)){
        ARRAY_DICT_NODOS[0].color = { background: "#008000", border: "#ff0000" }
    }
    var divEvaluar = document.getElementById("divEvaluar");
    divEvaluar.style.display = "";
    draw();
}

function aux_paint_end_estate() {
    var estadoFinal = document.getElementById("esadoFinal").value;
    ESTADO_ACEPTACION = estadoFinal.split(",")
    for (let i = 0; i < ESTADO_ACEPTACION.length; i++) {
        if (NODOS_ARRAY.includes(ESTADO_ACEPTACION[i].toUpperCase())) {
            for (let j = 0; j < ARRAY_DICT_NODOS.length; j++) {
                if (ARRAY_DICT_NODOS[j].label == ESTADO_ACEPTACION[i]) {
                    ARRAY_DICT_NODOS[j].color = "#ff0000"
                }
            }
        }
    }
}

function evaluar_beta() {
    var pivote = 0; // para saber en que estado estoy
    //get string
    const cadena = document.getElementById("cadena").value;
    //convert string to array
    const array_cadena = cadena.split("");
    for (let i = 0; i < array_cadena.length; i++) {
        for (let j = 0; j < EDGES.length; j++) {
            if (! ARRAY_ALFABETO.includes(array_cadena[i])) {
                alert("ATENCION: en la posicion: "+(i+1)+" de la CADENA se encuentra el simbolo: "+array_cadena[i]+" El cual no pertenece al ALFABETO");
                return;
            }
            if (EDGES[j].label == array_cadena[i] & EDGES[j].from == pivote) {
                pivote = EDGES[j].to;
                break;
            }
        }
    }
    const end_state = NODOS_ARRAY[pivote]
    if (ESTADO_ACEPTACION.includes(end_state)) {
        alert("Cadena aceptada, termina en el esatdo "+end_state)
    }else{
        alert("Cadena rechazada, termina en el esatdo "+end_state)
    }
}

function stillCorrect(cadena){
    var aux_matches = 0;
    for(let i = 0; i < ARRAY_ALFABETO.length; i++){
        if(cadena.slice(0,ARRAY_ALFABETO[i].length) == ARRAY_ALFABETO[i] ){
            aux_matches = aux_matches + 1;
            break;
        }
    }
    if(aux_matches == 0){
        return false;
    }else{
        return true;
    }
}

function evaluar(){
    var state = 0;
    var cadena_recorrida = "";
    var label_count;
    var cadena = document.getElementById("cadena").value;
    console.log(EDGES)
    while (cadena.length != 0){
        if(stillCorrect(cadena)){
            for (let j = 0; j < EDGES.length; j++){
                if(EDGES[j].to == -1 & EDGES[j].from == state){
                    alert("El estado: "+state+ " No cuenta con la arista que deberia tener el simbolo: "+EDGES[j].label);
                    return;
                }
                label_count = EDGES[j].label.length.toString();
                if(EDGES[j].label == cadena.slice(0,label_count) & EDGES[j].from == state){
                    state = EDGES[j].to;
                    cadena_recorrida = cadena_recorrida + cadena.slice(0,label_count);
                    cadena = cadena.slice(label_count)
                    break;
                }
            }
        }else {
            alert("La cadena cuenta con simbolos que no concuerdan con el alfabeto definido");
            alert("Cadena Recorrida con exito: "+cadena_recorrida);
            alert("Cadena Faltante: "+cadena+"... Apartir de la primera posicion se encuentra el error");
            cadena = "";
            return;
        }
    }
    const end_state = NODOS_ARRAY[state]
    if (ESTADO_ACEPTACION.includes(end_state)) {
        alert("Cadena aceptada, termina en el esatdo "+end_state)
    }else{
        alert("Cadena rechazada, termina en el esatdo "+end_state)
    }
}

function reset(){

    
}


//!!!-------------------------------------------------------------------------------------!!!

//Show Modal
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})
